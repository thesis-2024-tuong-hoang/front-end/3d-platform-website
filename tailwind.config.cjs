/* eslint-disable prettier/prettier */
// eslint-disable-next-line no-undef
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  plugins: [
    // ...
    require('@tailwindcss/line-clamp'),
  ],
  theme: {
    screens: {
      'xs': '390px',
      // => @media (min-width: 390px) { ... }

      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    },
    extend: {
      fontFamily: {
        default:
          '"Inter", -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji"',
      },
      colors: {
        'search-bar': '#f1f1f2',
        'primary': '#fb923c'
      },
      textColor: {
        "system-highlight": "var(--text-system-highlight)",
        "system-default": "var(--text-system-default)",
      },
      backgroundColor: {
        'system-default': "var(--bg-system-default)",
        'shadow': "linear-gradient(to bottom, transparent 0%, transparent 8.1%, rgba(0,0,0,0.001) 15.5%, rgba(0,0,0,0.003) 22.5%, rgba(0,0,0,0.005) 29%, rgba(0,0,0,0.008) 35.3%, rgba(0,0,0,0.011) 41.2%, rgba(0,0,0,0.014) 47.1%, rgba(0,0,0,0.016) 52.9%, rgba(0,0,0,0.019) 58.8%, rgba(0,0,0,0.022) 64.7%, rgba(0,0,0,0.025) 71%, rgba(0,0,0,0.027) 77.5%, rgba(0,0,0,0.029) 84.5%, rgba(0,0,0,0.03) 91.9%, rgba(0,0,0,0.03) 100%)",

        // Theme
        'theme-color-blue': 'var(--theme-color-blue)',
        'theme-color-yellow': 'var(--theme-color-yellow)',
        'theme-color-red': 'var(--theme-color-red)',
        'theme-color-purple': 'var(--theme-color-purple)',
        'theme-color-orange': 'var(--theme-color-orange)',
        'theme-color-green': 'var(--theme-color-green)',

        'theme-background-light': 'var(--theme-background-light)',
        'theme-background-dim': 'var(--theme-background-dim)',
        'theme-background-dark': 'var(--theme-background-dark)',
      },
      borderColor: {
        'system-default': "var(--border-system-default)",
      },
      boxShadow: {
        'header': ' 0px 1px 1px rgba(0, 0, 0, 0.12)',
      }
    },
  },
  corePlugins: {
    preflight: false,
  },
}
