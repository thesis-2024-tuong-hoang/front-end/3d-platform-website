import { useCallback, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import axios from 'axios'
import FormComponent from '~root/components/Form'
import CustomInput from '~root/components/CustomInput'
import { BASE_3D_PROCESSOR_API_URL, BASE_API_URL } from '~root/constants'
import * as Yup from 'yup'
import CustomButton from '~root/components/CustomButton'
import { useAppSelector } from '~root/config/redux/reduxHooks'
import { FcApproval } from 'react-icons/fc'
import { IoCloseOutline } from 'react-icons/io5'
import { Link } from '@mui/material'

const UploadSchema = Yup.object().shape({
    name: Yup.string().required('This field is required'),
    description: Yup.string().required('This field cannot be empty'),
})

interface IUploadValues {
    name: string
    description: string
}

const DEFAULT_UPLOAD_VALUE: IUploadValues = {
    name: '',
    description: '',
}

export default function UploadScreenVariant() {
    const [successfulUpload, setSuccessfulUpload] = useState<boolean>(false)
    const [myFiles, setMyFiles] = useState<any[]>([])
    const currentUserId = useAppSelector((state) => state.auth.currentUser)
    const authObj = useAppSelector((state) => state.auth)

    const onDrop = useCallback(
        (acceptedFiles: any) => {
            setMyFiles([...myFiles, ...acceptedFiles])
        },
        [myFiles],
    )

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
    })

    const handleCloseSuccessModal = () => {
        setSuccessfulUpload(false)
    }

    const uploadFiles = (data: any) => {
        const textureFiles: any[] = []

        const otherFiles: any[] = myFiles.filter((file: any) => {
            if (
                file.name.includes('.bin') ||
                file.name.includes('.gltf') ||
                file.name.includes('.txt') ||
                file.name.includes('-')
            ) {
                return file
            } else {
                textureFiles.push(file)
            }
        })

        return {
            'other-files': otherFiles,
            textures: textureFiles,
        }
    }

    const handleSubmit = async (values: any) => {
        try {
            const filesData = await handleUpload()
            const modelObj = { ...values, userId: currentUserId }

            const modelFilename = modelObj.name.toLowerCase().split(' ').join('-')

            // const authorizeResponse = await axios({
            //     method: 'POST',
            //     baseURL: BASE_API_URL + `/auth/authorize`,
            //     headers: {
            //         'Authorization-1': authObj.accessToken,
            //         'Authorization-2': authObj.refreshToken,
            //     },
            //     withCredentials: true,
            // })

            const fileResponse = await axios({
                method: 'POST',
                baseURL: BASE_3D_PROCESSOR_API_URL + `/process-file/upload/${modelFilename}`,
                data: filesData,
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            })

            const dataResponse = await axios({
                method: 'POST',
                baseURL: BASE_API_URL + `/process-data/upload-model`,
                data: modelObj,
                headers: {
                    'Content-Type': 'application/json',
                },
            })

            if (fileResponse.status === 200 && dataResponse.status === 201) {
                setSuccessfulUpload(true)
            }
        } catch (error) {
            console.log(error)
        }
    }

    const packFiles = (acceptedFiles: any) => {
        const data = new FormData()

        myFiles.forEach((file: any) => {
            data.append(`file-${file.path}`, file, file.path)
        })

        return data
    }

    const handleUpload = async () => {
        if (myFiles.length) {
            const data = packFiles(myFiles)
            return uploadFiles(data)
        }
    }

    const removeFile = (removeFile: any) => () => {
        setMyFiles((currentFiles) => {
            return currentFiles.filter((file) => file.name != removeFile.name)
        })
    }

    const removeAll = () => {
        setMyFiles([])
    }

    const renderFileList = () => (
        <ul className='list-none overflow-scroll max-h-[360px]'>
            {myFiles.map((f: any, i: number) => (
                <li
                    key={i}
                    className='flex items-center justify-between min-w-[400px] hover:bg-primary/10 select-none border-solid border-[1px] border-black/20 p-2 px-4 pr-2 rounded-md mb-2'
                >
                    <p>{`${i + 1}. ${f.name} - ${f.type}`}</p>
                    <IoCloseOutline
                        className='text-2xl cursor-pointer hover:text-primary'
                        onClick={removeFile(f)}
                    />
                </li>
            ))}
        </ul>
    )

    return (
        <div className=' w-full flex-grow relative flex justify-center items-center'>
            <img
                className='w-full h-full absolute z-0'
                src='https://wallpaperaccess.com/full/2909129.jpg'
                alt='upload-background'
            />
            <div className='w-2/3 h-fit p-6 my-20 absolute z-10  bg-white justify-center items-center gap-6 flex rounded-md '>
                {!successfulUpload && (
                    <div className='w-full h-fit flex flex-col justify-start items-center '>
                        <div className='w-full p-8 pt-4  flex flex-col justify-start items-center '>
                            <div className='text-center text-neutral-700 text-5xl font-semibold'>
                                Upload your model
                            </div>
                        </div>
                        <div className='w-full flex justify-between '>
                            <FormComponent
                                className='w-full pl-5 max-w-[423px]'
                                initialValues={DEFAULT_UPLOAD_VALUE}
                                validationSchema={UploadSchema}
                                onSubmit={handleSubmit}
                            >
                                <CustomInput
                                    name='name'
                                    label='Name'
                                    type='text'
                                    style='TextField'
                                />
                                <CustomInput
                                    name='description'
                                    label='Description'
                                    type='text'
                                    style='TextField'
                                />
                                <div className='w-full h-36 relative mt-6'>
                                    <div
                                        {...getRootProps({
                                            className:
                                                'w-full h-full flex justify-center p-6 w-full h-full flex justify-center items-center border-2 border-dashed rounded-lg border-neutral-700 hover:bg-primary/10 cursor-pointer transition-colors',
                                        })}
                                    >
                                        <input {...getInputProps()} />
                                        <p>
                                            {isDragActive
                                                ? 'Drop the files here ...'
                                                : 'Drag and drop some files here'}
                                        </p>
                                    </div>
                                </div>

                                <CustomButton
                                    className='w-full rounded-md text-xs py-4 font-semibold mt-5 bg-primary/90 hover:cursor-pointer hover:bg-primary transition-colors'
                                    title='UPLOAD'
                                    submit
                                />
                            </FormComponent>
                            <div
                                className='flex-grow-0 h-[448px] w-[1px] mx-10'
                                style={{
                                    background:
                                        'linear-gradient(180deg,hsla(0,0%,100%,0),rgba(138,138,160,.3) 50.52%,hsla(0,0%,100%,0))',
                                }}
                            ></div>
                            <div className='flex flex-col '>
                                {myFiles.length === 0 && (
                                    <div className='h-full flex flex-col items-center justify-center px-5 pr-16'>
                                        <img
                                            className='w-full select-none'
                                            src='https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png'
                                            alt='empty file'
                                        />
                                        <h4>Please upload 3D model files</h4>
                                    </div>
                                )}
                                {myFiles.length !== 0 && (
                                    <div className='mt-3 self-stretch mr-2'>
                                        <div className='flex items-center justify-between'>
                                            <h4 className='mb-4'>Files uploaded:</h4>
                                            <span
                                                className='mb-4 hover:text-primary cursor-pointer text-sm'
                                                onClick={removeAll}
                                            >
                                                Remove all
                                            </span>
                                        </div>
                                        {renderFileList()}
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                )}
                {successfulUpload && (
                    <div className=' h-fit flex flex-col justify-center items-center'>
                        {/* <div className='w-full flex justify-end text-4xl hover:text-primary cursor-pointer transition-colors'>
                            <IoCloseOutline onClick={handleCloseSuccessModal} />
                        </div> */}
                        <FcApproval className='text-9xl mb-8' />
                        <h4 className='text-4xl'>Upload Completed</h4>
                        <p className='mt-4 text-xl'>Your files have been uploaded successfully !</p>
                        <Link
                            style={{ color: 'white', marginTop: 40 }}
                            href='/'
                            underline='none'
                            className=' block mt-10 p-4 rounded-md bg-primary font-medium cursor-pointer'
                        >
                            GO TO HOME PAGE
                        </Link>
                    </div>
                )}
            </div>
        </div>
    )
}
