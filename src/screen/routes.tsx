import React from 'react'
import { Routes, Route } from 'react-router-dom'
import { LOCAL_STORAGE_TOKEN_LOGIN } from '~root/constants'
import { Navigate } from 'react-router-dom'
import HomeScreen from './HomeScreen'
import UploadScreenVariant from './UploadScreen'
import UserProfileScreen from './UserProfileScreen'
import DetailedModalScreen from './DetailedModelSrceen'
import DefaultLayout from '~root/layouts/DefaultLayout'
import AuthenticationLayout from '~root/layouts/AuthenticationLayout'
import LoginScreen from './AuthenticationScreen/LoginScreen'
import SignupScreen from './AuthenticationScreen/SignupScreen'
import EditUserScreen from './EditUserScreen'

export interface IRoutes {
    path: string
    requiredLogin: boolean
    element: React.ReactNode
    Layout?: React.FunctionComponent<any> | React.FC<any>
}

const routesList: IRoutes[] = [
    {
        path: '/',
        requiredLogin: false,
        element: <HomeScreen />,
        Layout: DefaultLayout,
    },
    {
        path: '/:user',
        requiredLogin: false,
        element: <UserProfileScreen />,
        Layout: DefaultLayout,
    },
    {
        path: '/edit-profile',
        requiredLogin: true,
        element: <EditUserScreen />,
        Layout: DefaultLayout,
    },
    {
        path: '/upload',
        requiredLogin: true,
        element: <UploadScreenVariant />,
        Layout: DefaultLayout,
    },
    {
        path: '/login',
        requiredLogin: false,
        element: <LoginScreen />,
        Layout: AuthenticationLayout,
    },
    {
        path: '/signup',
        requiredLogin: false,
        element: <SignupScreen />,
        Layout: AuthenticationLayout,
    },
    {
        path: '/3d-models/:modelName',
        requiredLogin: false,
        element: <DetailedModalScreen />,
        Layout: DefaultLayout,
    },
]

export const RoutesList = () => {
    return (
        <Routes>
            {routesList.map((route, idx) => {
                let Element = route.element
                if (route.Layout) {
                    Element = <route.Layout>{Element}</route.Layout>
                }
                if (route.requiredLogin) {
                    Element = <RequiredLogin>{Element}</RequiredLogin>
                }
                return <Route key={route.path} path={route.path} element={Element} />
            })}
        </Routes>
    )
}

function RequiredLogin(props: { children?: React.ReactNode }) {
    const token =
        localStorage.getItem(LOCAL_STORAGE_TOKEN_LOGIN) ||
        localStorage.getItem('sb-xpvurtyfnuyuvkehgqoo-auth-token')

    if (token) {
        return <>{props.children}</>
    } else {
        return <Navigate to='/login' replace />
    }
}
