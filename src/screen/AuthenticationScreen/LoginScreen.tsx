/* eslint-disable react/no-unknown-property */
import { FcGoogle } from 'react-icons/fc'
import { useNavigate, useLocation } from 'react-router-dom'
import LoginModal from '~root/components/AuthModal/LoginModal'
import { supabase } from '~root/config/supabase'
import { Canvas } from '@react-three/fiber'
import { OrbitControls, Environment, Bounds } from '@react-three/drei'
import Model from '~root/components/Model'
import { useEffect } from 'react'
import { LOCAL_STORAGE_TOKEN_LOGIN } from '~root/constants'
import { useAppSelector } from '~root/config/redux/reduxHooks'

const handleSignInWithGoogle = async () => {
    await supabase.auth.signInWithOAuth({
        provider: 'google',
    })
}

export default function LoginScreen() {
    const navigate = useNavigate()
    const location = useLocation()
    const isAuthenticated = useAppSelector((state) => state.auth.isAuthenticated)

    useEffect(() => {
        const googleToken = localStorage.getItem('sb-xpvurtyfnuyuvkehgqoo-auth-token')
        const serverToken = localStorage.getItem(LOCAL_STORAGE_TOKEN_LOGIN)

        if ((!googleToken || !serverToken) && isAuthenticated) {
            navigate('/', { replace: false })
        }
    }, [isAuthenticated, navigate])

    const navigateToHomePage = () => {
        navigate('/', { replace: false })
    }

    return (
        <div className='w-full flex items-center h-screen'>
            <div className='w-3/5 h-full bg-white'>
                <div className='h-full flex flex-col items-center justify-center relative'>
                    <div className='absolute top-6 left-6 w-full flex items-center justify-start'>
                        <div
                            className='flex items-center text-2xl hover:cursor-pointer '
                            onClick={navigateToHomePage}
                        >
                            <h3 className='text-orange-400 mr-2'>3D</h3>
                            <h3 className=''>Platform</h3>
                        </div>
                    </div>
                    <div className='flex flex-col items-center'>
                        <div className='flex flex-col items-center'>
                            <h3 className='font-mediums text-6xl'>Login to Your Account</h3>
                            {location.state?.message && (
                                <p className='text-green-400 mt-6 text-lg'>
                                    {location.state?.message}
                                </p>
                            )}
                            <p className='my-5 text-xl text-black/60'>
                                Login using social networks
                            </p>
                        </div>
                        <span
                            className='flex items-center justify-start w-1/2 mb-4'
                            onClick={handleSignInWithGoogle}
                        >
                            <div className='flex items-center justify-center border-[1px] w-[96%] p-3 rounded-md border-black/20 border-solid text-black/50 hover:text-black hover:border-black cursor-pointer transition-colors'>
                                <FcGoogle className=' text-xl mr-2' />
                                <p className='text-base font-medium'>Google</p>
                            </div>
                        </span>
                        <div className='w-full flex items-center justify-center'>
                            <div className='h-[1px] w-1/3 bg-black/20'></div>
                            <p className='mx-4'>OR</p>
                            <div className='h-[1px] w-1/3 bg-black/20'></div>
                        </div>
                        <LoginModal />
                    </div>
                </div>
            </div>
            <div className='flex-1 h-full bg-slate-600 relative'>
                <Canvas
                    className='w-10 h-10'
                    // camera={{ position: [10, 0, 20], zoom: 10, near: 1, far: 1000 }}
                    camera={{
                        position: [5, 5, 10],
                        far: 1000,
                        zoom: 1,
                    }}

                    // camera={{ rotation: [0, 0, 0], position: [0, 0, 0] }}
                >
                    {/* <PerspectiveCamera fov={50} /> */}
                    <Environment preset='studio' background blur={0.9} />
                    <group>
                        <Bounds fit margin={2.2}>
                            <Model
                                url={`https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/3D-models/bird-orange/scene.gltf`}
                            />
                        </Bounds>
                    </group>
                    <OrbitControls enablePan={false} makeDefault />
                    {/* <Stats /> */}
                    {/* <axesHelper args={[5]} /> */}
                    {/* <gridHelper /> */}

                    {/* <Grid /> */}
                </Canvas>
                <div className=' absolute bottom-8 left-8'>
                    <div className='flex items-center'>
                        <img
                            className='w-12 h-12 mr-4'
                            src='https://image.api.playstation.com/cdn/UP1018/CUSA00133_00/Dz5i57ktol8qBJsWEVdjNy8wj3Tc5fF0Y2MyX0Poi4lAniOaUpfjH5IU2vVejxey.png?w=440&thumb=false'
                            alt='batman'
                        />
                        <p className='text-white'>Tuong Hoang</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
