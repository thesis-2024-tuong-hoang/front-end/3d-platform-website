/* eslint-disable react/no-unknown-property */
import { useNavigate } from 'react-router-dom'
import SignupModal from '~root/components/AuthModal/SignupModal'
import { Canvas } from '@react-three/fiber'
import { OrbitControls, Environment, Bounds } from '@react-three/drei'
import Model from '~root/components/Model'
import { LOCAL_STORAGE_TOKEN_LOGIN } from '~root/constants'
import { useEffect } from 'react'

export default function SignupScreen() {
    const navigate = useNavigate()

    useEffect(() => {
        const googleToken = localStorage.getItem('sb-xpvurtyfnuyuvkehgqoo-auth-token')
        const serverToken = localStorage.getItem(LOCAL_STORAGE_TOKEN_LOGIN)

        if (googleToken || serverToken) {
            navigate('/', { replace: false })
        }
    }, [navigate])

    const navigateToHomePage = () => {
        navigate('/', { replace: false })
    }

    return (
        <div className='w-full flex items-center h-screen'>
            <div className='w-3/5 h-full bg-white'>
                <div className=' h-full flex flex-col items-center justify-center relative'>
                    <div className='absolute top-6 left-6 w-full flex items-center justify-start'>
                        <div
                            className='flex items-center text-2xl hover:cursor-pointer '
                            onClick={navigateToHomePage}
                        >
                            <h3 className='text-orange-400 mr-2'>3D</h3>
                            <h3 className=''>Platform</h3>
                        </div>
                    </div>
                    <div className='flex flex-col items-center'>
                        <div className='flex flex-col items-center mb-10'>
                            <h3 className='font-mediums text-6xl'>Create your Account</h3>
                        </div>
                        <SignupModal />
                    </div>
                </div>
            </div>
            <div className='flex-1 h-full bg-slate-600 relative'>
                <Canvas
                    className='w-10 h-10'
                    camera={{
                        position: [5, 5, 10],
                        far: 1000,
                        zoom: 1,
                    }}
                >
                    <Environment preset='studio' background blur={0.9} />
                    <group>
                        <Bounds fit margin={2.2}>
                            <Model
                                url={`https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/3D-models/bird-orange/scene.gltf`}
                            />
                        </Bounds>
                    </group>
                    <OrbitControls enablePan={false} makeDefault />
                </Canvas>
                <div className=' absolute bottom-8 left-8'>
                    <div className='flex items-center'>
                        <img
                            className='w-12 h-12 mr-4'
                            src='https://image.api.playstation.com/cdn/UP1018/CUSA00133_00/Dz5i57ktol8qBJsWEVdjNy8wj3Tc5fF0Y2MyX0Poi4lAniOaUpfjH5IU2vVejxey.png?w=440&thumb=false'
                            alt='batman'
                        />
                        <p className='text-white'>Tuong Hoang</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
