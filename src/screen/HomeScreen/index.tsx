import ModelCard from '~root/components/ModelCard'
import { useCallback, useLayoutEffect, useState } from 'react'
import axios from 'axios'
import { BASE_API_URL } from '~root/constants'
import { IModelCard } from '../DetailedModelSrceen'

export default function HomeScreen() {
    const [models, setModels] = useState<any[]>([])

    const getAllModels = useCallback(async () => {
        const response = await axios({
            method: 'GET',
            baseURL: `${BASE_API_URL}/process-data/3d-models/all`,
        })
        setModels(response.data.models)
    }, [])

    useLayoutEffect(() => {
        getAllModels()
    }, [getAllModels])

    return (
        <div className='w-full flex flex-col pb-10 bg-white'>
            <div className='w-full h-fit relative  rounded-md overflow-hidden px-8 mt-8'>
                <img
                    src='https://images.pexels.com/photos/8347500/pexels-photo-8347500.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2'
                    alt='slide'
                    className='block w-full h-[200px]  rounded-md '
                />
                <div className='absolute w-1/2 top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2'>
                    <div className='text-white flex items-center justify-around'>
                        <h3 className='text-5xl font-bold'>3D Platform</h3>
                        <div className='w-[2px] h-20 bg-white'></div>
                        <div className='flex flex-col'>
                            <p className='text-lg'>The internet’s source for 3D models.</p>
                            <p className='text-lg'>Powered by creators everywhere.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='relative w-full bg-white py-8 px-8 min-h-screen'>
                <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6  '>
                    {models.map((model: IModelCard) => (
                        <ModelCard key={model.id} model={model} />
                    ))}
                </div>
            </div>
        </div>
    )
}
