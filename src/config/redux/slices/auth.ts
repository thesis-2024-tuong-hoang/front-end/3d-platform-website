import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { RootState } from '..'
import { BASE_API_URL } from '~root/constants'

interface IInitialState {
    currentUser: string
    isLoading: boolean
    isAuthenticated: boolean
    accessToken: string
    refreshToken: string
    errorMessage: string
    accountType: 'google' | 'normal' | ''
}

export const initialState: IInitialState = {
    currentUser: '',
    isLoading: false,
    isAuthenticated: false,
    accessToken: '',
    refreshToken: '',
    errorMessage: '',
    accountType: '',
}

export const login = createAsyncThunk(
    'user/login',

    async (data: any, { rejectWithValue }) => {
        const response = await fetch(`${BASE_API_URL}/auth/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })

        const jsonData = await response.json()

        if (response.status < 200 || response.status >= 300) {
            return rejectWithValue(jsonData)
        }

        console.log('haha: ', jsonData)
        return jsonData
    },
)

const auth = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: (state, action) => {
            state.currentUser = action.payload.currentUser
            state.isLoading = false
            state.isAuthenticated = true
            state.accessToken = action.payload.accessToken
            state.refreshToken = action.payload.refreshToken
            state.accountType = action.payload.accountType
            state.errorMessage = ''
        },
        logout: (state) => {
            state.currentUser = ''
            state.errorMessage = ''
            state.isAuthenticated = false
            state.isLoading = false
            state.accountType = ''
            state.accessToken = ''
            state.refreshToken = ''
        },
    },
    extraReducers: (builder) => {
        builder.addCase(login.pending, (state) => {
            state.isLoading = true
        })

        builder.addCase(login.fulfilled, (state, action) => {
            state.isLoading = false
            state.isAuthenticated = true
            state.currentUser = action.payload.userId
            state.accessToken = action.payload.accessToken
            state.refreshToken = action.payload.refreshToken
            state.accountType = 'normal'
            state.errorMessage = ''
        })

        builder.addCase(login.rejected, (state, action: any) => {
            state.isLoading = false
            state.errorMessage = action.payload.errorMessage
        })
    },
})

export const authStore = (state: RootState) => state.auth // get state
export const AuthActions = auth.actions
export default auth.reducer
