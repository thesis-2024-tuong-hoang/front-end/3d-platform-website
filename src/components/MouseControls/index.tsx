import { CameraControls } from '@react-three/drei'
import { useRef } from 'react'
import { useThree, useFrame } from '@react-three/fiber'

export default function MouseControls() {
    const ref = useRef() as any
    const camera = useThree((state) => state.camera)
    const gl = useThree((state) => state.gl)
    useFrame((state, delta) => {
        // update camera angles according to mouse position
        ref.current!.azimuthAngle = -state.mouse.x
        ref.current!.polarAngle = Math.PI / 2 + state.mouse.y
        ref.current!.update(delta)
    })
    return <CameraControls ref={ref} args={[camera, gl.domElement]} />
}
