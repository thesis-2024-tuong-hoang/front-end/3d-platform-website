import { IoEyeOutline } from 'react-icons/io5'
import { IoIosStarOutline } from 'react-icons/io'
import { useNavigate } from 'react-router-dom'
import { IModelCard } from '~root/screen/DetailedModelSrceen'
import { useCallback, useLayoutEffect, useState } from 'react'
import axios from 'axios'
import { BASE_API_URL } from '~root/constants'

interface IModelHorizontalCardProps {
    model: IModelCard
}

export default function ModelHorizontalCard(props: IModelHorizontalCardProps) {
    const { model } = props
    const [user, setUser] = useState<any>()
    const navigate = useNavigate()

    const getUser = useCallback(async () => {
        const response = await axios({
            method: 'POST',
            baseURL: `${BASE_API_URL}/process-data/user`,
            data: {
                userId: model.user_id,
            },
        })
        if (response.data.user) {
            setUser(response.data.user)
        }
    }, [model.user_id])

    useLayoutEffect(() => {
        getUser()
    }, [getUser])

    const handleNavigateToModel = () => {
        navigate(`/3d-models/${model.file_name}`, { replace: false })
        location.reload()
    }

    return (
        <div
            key={model.name}
            className='w-full flex h-24 bg-white mb-4 shadow-md hover:shadow-xl transition-all cursor-pointer no-underline'
            onClick={handleNavigateToModel}
        >
            <img
                className='w-48 '
                src={`https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/3D-models/${model.file_name}/${model.file_name}.png`}
                alt=''
            />
            <div className='w-3/5 flex flex-col justify-between py-2 px-[10px]'>
                <div className='flex flex-col text-black/80'>
                    <h4 className='text-sm'>{user?.username}</h4>
                    <p className='text-xs'>{user?.email}</p>
                </div>
                <div className='flex items-center'>
                    <div className='flex items-center text-black/40 mr-2'>
                        <IoEyeOutline className='mr-1' />
                        <p className='text-xs'>{model.viewed}</p>
                    </div>
                    {/* <div className='flex items-center text-black/40 mr-2'>
                        <HiOutlineChatBubbleLeft className='mr-1' />
                        <p className='text-xs'>{model.}</p>
                    </div> */}
                    <div className='flex items-center text-black/40 mr-2'>
                        <IoIosStarOutline className='mr-1' />
                        <p className='text-xs'>{model.stars}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
