import FormComponent from '../Form'
import CustomInput from '../CustomInput'
import CustomButton from '../CustomButton'
import { useNavigate } from 'react-router-dom'
import { Field } from 'formik'
import * as Yup from 'yup'
import { BASE_API_URL } from '~root/constants'

const SignupSchema = Yup.object().shape({
    // firstName: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Required'),
    username: Yup.string()
        .min(2, 'Username is too short!')
        .max(50, 'Username is too long!')
        .required('This field is required'),
    email: Yup.string().email('This email is invalid').required('This field is required'),
    password: Yup.string()
        .min(6, 'Password must be greater than 6 characters')
        .required('This field cannot be empty'),
})

interface ISignupValues {
    username: string
    email: string
    password: string
}

const DEFAULT_SIGNUP_VALUE: ISignupValues = {
    username: '',
    email: '',
    password: '',
}

export default function SignupModal() {
    const navigate = useNavigate()

    const handleNavigateToLogIn = () => {
        navigate('/login', { replace: false })
    }

    return (
        <div className='bg-white rounded-md w-full  flex flex-col'>
            <FormComponent
                initialValues={DEFAULT_SIGNUP_VALUE}
                validationSchema={SignupSchema}
                onSubmit={async (values) => {
                    const response = await fetch(`${BASE_API_URL}/auth/register`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(values),
                    })
                    if (response.status === 201) {
                        navigate('/login', {
                            replace: false,
                            state: { message: 'Congratulation, your account has been created!' },
                        })
                    }
                    console.log(response)
                }}
            >
                <CustomInput
                    name='username'
                    label='Choose a username'
                    type='text'
                    style='TextField'
                />
                <CustomInput name='email' label='Email' type='email' style='TextField' />
                <CustomInput
                    name='password'
                    label='Create password'
                    type='password'
                    style='TextField'
                />
                <span className='flex items-center text-sm my-4'>
                    <Field
                        type='checkbox'
                        name='checked'
                        value='One'
                        className='accent-primary w-4 h-4 mr-2'
                    />
                    <p className='text-black/70'>
                        I agree to the
                        <strong className='font-normal text-primary'> Terms of Use</strong> and
                        <strong className='font-normal text-primary'> Privacy Policy</strong>
                    </p>
                </span>
                <p className='max-w-xl  text-justify text-xs text-black/40'>
                    By creating an account, your username becomes public and can be read by anyone
                    who visits the website. Do not include sensitive data like IDs, credentials, or
                    non-public information.{' '}
                    <strong className='text-primary font-normal cursor-pointer'>
                        Learn how to edit your username. Learn how to delete your account.
                    </strong>
                </p>
                <CustomButton
                    className='w-full rounded-md text-xs py-4 font-semibold mt-6 bg-primary/90 hover:cursor-pointer hover:bg-primary transition-colors'
                    title='CREATE ACCOUNT'
                    submit
                />
            </FormComponent>

            <div className='flex items-center justify-center mt-8 mb-8'>
                <p className='text-sm mr-2'>Already have an account?</p>
                <span
                    className='text-sm font-medium text-primary hover:underline cursor-pointer'
                    onClick={handleNavigateToLogIn}
                >
                    Log in here
                </span>
            </div>
        </div>
    )
}
