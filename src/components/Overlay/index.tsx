import { ReactNode } from 'react'

interface IOverlayProps {
    children: ReactNode
}

export default function Overlay({ children }: IOverlayProps) {
    return (
        <div className='fixed top-0 left-0 right-0 bottom-0 bg-black/20 flex items-center justify-center z-30'>
            {children}
        </div>
    )
}
