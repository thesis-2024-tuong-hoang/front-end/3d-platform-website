/* eslint-disable react/no-unknown-property */
import { useGLTF } from '@react-three/drei'

interface IModelProps {
    url: string
}

export default function Model(props: IModelProps) {
    const { scene } = useGLTF(props.url)
    return <primitive object={scene} {...props} />
}
