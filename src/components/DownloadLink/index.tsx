import { MdOutlineFileDownload } from 'react-icons/md'
import { supabase } from '~root/config/supabase'
import { BlobReader, BlobWriter, ZipWriter } from '@zip.js/zip.js'
import { useLayoutEffect, useState, useCallback } from 'react'
import { PiDownloadSimpleLight } from 'react-icons/pi'

interface IDownloadLinkProps {
    modelFolder: string
    downloaded: number
    modelId: string
    mode: 'short' | 'long'
}

export default function DownloadLink(props: IDownloadLinkProps) {
    const { modelFolder, downloaded, modelId, mode } = props

    const [downloadUrl, setDownloadUrl] = useState<string>()

    const getAllFiles = useCallback(async () => {
        // Get a list of all the files in the path /my-bucket/images
        const { data: files, error: fileError } = await supabase.storage
            .from('3D-models')
            .list(modelFolder)
        const { data: textureFiles, error: textureError } = await supabase.storage
            .from(`3D-models`)
            .list(`${modelFolder}/textures`)

        if (fileError || textureError) {
            throw fileError || textureError
        }

        // If there are no files in the folder, throw an error
        if (!files || !files.length) {
            throw new Error('No files to download')
        }

        const allFiles = [...files, ...textureFiles].filter(
            (file) => file.name !== 'textures' && file.name !== `${modelFolder}.png`,
        )

        return allFiles
    }, [modelFolder])

    const downloadAllFiles = useCallback(async () => {
        const files = await getAllFiles()
        const promises: any[] = []

        // Download each file in the folder
        files.forEach((file) => {
            if (file.name.includes('.png') || file.name.includes('.jpeg')) {
                promises.push(
                    supabase.storage
                        .from('3D-models')
                        .download(`${modelFolder}/textures/${file.name}`),
                )
            } else {
                promises.push(
                    supabase.storage.from('3D-models').download(`${modelFolder}/${file.name}`),
                )
            }
        })

        // Wait for all the files to download
        const response = await Promise.allSettled(promises)

        // Map the response to an array of objects containing the file name and blob
        const downloadedFiles = response.map((result, index) => {
            if (result.status === 'fulfilled') {
                return {
                    name: files[index].name,
                    blob: result.value.data,
                }
            }
        })

        return downloadedFiles
    }, [getAllFiles, modelFolder])

    const createZip = useCallback(async () => {
        const downloadedFiles = await downloadAllFiles()
        // Create a new zip file
        const zipFileWriter = new BlobWriter('application/zip')
        const zipWriter = new ZipWriter(zipFileWriter, { bufferedWrite: true })

        // Add each file to the zip file
        downloadedFiles.forEach((downloadedFile) => {
            if (downloadedFile?.name.includes('.bin') || downloadedFile?.name.includes('.gltf')) {
                zipWriter.add(downloadedFile.name, new BlobReader(downloadedFile.blob))
            } else {
                zipWriter.add(
                    `textures/${downloadedFile?.name}`,
                    new BlobReader(downloadedFile?.blob),
                )
            }
        })

        // Download the zip file
        const url = URL.createObjectURL(await zipWriter.close())
        setDownloadUrl(url)
    }, [downloadAllFiles])

    const AddModelDownloadNumber = async () => {
        await supabase
            .from('models')
            .update({ downloaded: downloaded + 1 })
            .eq('id', modelId)
    }

    useLayoutEffect(() => {
        createZip()
    }, [createZip])

    return (
        <>
            {mode === 'long' && (
                <div
                    className='flex items-center text-blue-400 mt-2 cursor-pointer border-solid border-b-[1px] border-black/20 pb-2'
                    onClick={AddModelDownloadNumber}
                >
                    <MdOutlineFileDownload className=' text-xl mr-1' />
                    <a
                        href={downloadUrl}
                        download={`${modelFolder}.zip`}
                        className='text-sm no-underline'
                    >
                        Download 3D Model
                    </a>
                </div>
            )}
            {mode === 'short' && (
                <div
                    className='flex justify-center items-center pl-2 lg:pl-2 xl:pl-3 hover:text-orange-400 transition-all cursor-pointer'
                    onClick={AddModelDownloadNumber}
                >
                    <a
                        href={downloadUrl}
                        download={`${modelFolder}.zip`}
                        className='no-underline text-black hover:text-primary'
                    >
                        <PiDownloadSimpleLight className='text-3xl' />
                    </a>
                </div>
            )}
        </>
    )
}
