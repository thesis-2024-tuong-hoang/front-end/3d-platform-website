import axios from 'axios'
import classNames from 'classnames'
import { useCallback, useLayoutEffect, useState } from 'react'
import { BASE_API_URL } from '~root/constants'
import { IModelCard } from '~root/screen/DetailedModelSrceen'
import DownloadLink from '../DownloadLink'
import { useNavigate } from 'react-router-dom'
import { IoEyeOutline } from 'react-icons/io5'
import { IoIosStarOutline } from 'react-icons/io'
import { Link } from '@mui/material'
import { DEFAULT_USER_AVATAR } from '~root/constants'

interface IModelCardProps {
    model: IModelCard
    disableAuthor?: boolean
}

export default function ModelCard(props: IModelCardProps) {
    const { model, disableAuthor } = props
    const [user, setUser] = useState<any>()
    const navigate = useNavigate()

    const navigateToProfile = () => {
        navigate(`${user?.slug}`, { replace: false })
    }

    const getUser = useCallback(async () => {
        const response = await axios({
            method: 'POST',
            baseURL: `${BASE_API_URL}/process-data/user`,
            data: {
                userId: model.user_id,
            },
        })
        if (response.data.user) {
            setUser(response.data.user)
        }
    }, [model.user_id])

    useLayoutEffect(() => {
        getUser()
    }, [getUser])

    return (
        <div className='col-span-1 pt-[78%] rounded-md bg-red-400 relative shadow-lg overflow-hidden '>
            <div className='flex flex-col bg-white absolute w-full h-full top-0 '>
                <Link
                    href={`3d-models/${model.file_name}`}
                    id='container'
                    className='w-full h-[78%] overflow-hidden flex justify-start items-center relative cursor-pointer'
                >
                    <img
                        id='image-mover'
                        className={classNames(
                            'block h-full w-full transition-all absolute top-0 left-0',
                        )}
                        src={`https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/3D-models/${model.file_name}/${model.file_name}.png`}
                        alt={model && model?.file_name}
                    />
                    <div className='absolute top-0 left-0 z-10 w-full h-full bg-gradient-to-t from-black/20 opacity-0 hover:opacity-100 transition-all'>
                        <div className='absolute right-3 top-3 bg-white p-2 rounded-sm flex items-center shadow-sm cursor-pointer text-black/50 hover:text-red-500 transition-all'>
                            <div className='flex items-center text-black/40 mr-2'>
                                <IoEyeOutline className='mr-1' />
                                <p className='text-xs'>{model.viewed}</p>
                            </div>
                            <div className='flex items-center text-black/40 mr-2'>
                                <IoIosStarOutline className='mr-1' />
                                <p className='text-xs'>{model.stars}</p>
                            </div>
                        </div>
                        <div className='absolute bottom-2 left-6'>
                            <h3 className='text-white text-base font-normal'>{model.name}</h3>
                        </div>
                    </div>
                </Link>
                <div className='flex-grow p-3 sm:p-3 md:p-3 lg:p-2 xl:p-3 flex items-center justify-between bg-white'>
                    {!disableAuthor && (
                        <div
                            className='flex justify-start items-center '
                            onClick={navigateToProfile}
                        >
                            <div className='w-10 h-10 sm:w-10 sm:h-10 md:w-8 md:h-8 xl:w-10 xl:h-10 flex justify-center items-center rounded-full overflow-hidden'>
                                <img
                                    className='block h-full '
                                    src={user?.avatar || DEFAULT_USER_AVATAR}
                                    alt='avatar'
                                />
                            </div>
                            <div className='ml-2 max-w-[192px] cursor-pointer'>
                                <h3 className='font-semibold text-sm truncate'>{user?.username}</h3>
                                <p className='font-normal text-xs text-black/50 line-clamp-1'>
                                    {user?.email}
                                </p>
                            </div>
                        </div>
                    )}

                    {disableAuthor && (
                        <div className='flex justify-start items-center '>
                            <div className='w-10 h-10 sm:w-10 sm:h-10 md:w-8 md:h-8 xl:w-10 xl:h-10 flex justify-center items-center rounded-full overflow-hidden'>
                                <img
                                    className='block h-full '
                                    src={user?.avatar || DEFAULT_USER_AVATAR}
                                    alt='avatar'
                                />
                            </div>
                            <div className='ml-2 max-w-[192px] '>
                                <h3 className='font-semibold text-sm truncate'>{user?.username}</h3>
                                <p className='font-normal text-xs text-black/50 line-clamp-1'>
                                    {user?.email}
                                </p>
                            </div>
                        </div>
                    )}

                    <div className='flex items-center'>
                        <div className='w-[1px] bg-black h-9'></div>
                        <DownloadLink
                            mode='short'
                            modelFolder={model.file_name}
                            modelId={model.id}
                            downloaded={model.downloaded}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}
