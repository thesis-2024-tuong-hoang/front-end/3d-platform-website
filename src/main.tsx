import ReactDOM from 'react-dom/client'
import App from './App'
import AppContextProvider from './contexts'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './config/redux'

// Theme
import './index.css'
import './theme/main-colors.css'
import './theme/theme-color-configs.css'
import './theme/theme-background-configs.css'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <AppContextProvider>
        <BrowserRouter>
            <Provider store={store}>
                <App />
            </Provider>
        </BrowserRouter>
    </AppContextProvider>,
)
