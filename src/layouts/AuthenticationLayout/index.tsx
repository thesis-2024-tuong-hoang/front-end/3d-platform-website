import React from 'react'

export default function AuthenticationLayout(props: { children: React.ReactNode }) {
    return <div className='w-full h-screen flex flex-col items-center'>{props.children}</div>
}
