import { forwardRef, useImperativeHandle, useState } from 'react'
import * as HiIcons from 'react-icons/hi2'

interface ISearchbarProps {
    searchFunction: (value: string) => void
}

export type Ref = HTMLInputElement | { clear: () => void }

const SearchBar = forwardRef<Ref, ISearchbarProps>((props, ref) => {
    const { searchFunction } = props

    const [enteredValue, setEnteredValue] = useState<string>('')

    useImperativeHandle(
        ref,
        () => ({
            clear: () => {
                setEnteredValue('')
            },
        }),
        [],
    )

    const handleSaveValue = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter' && enteredValue) {
            event.preventDefault()
            searchFunction(enteredValue)
        }
    }

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.value.startsWith(' ')) {
            return
        }
        setEnteredValue(event.target.value)
    }

    return (
        <div className='flex w-1/3 bg-search-bar items-center rounded-full border-[1px] border-transparent border-solid  hover:border-[#16182333] last:text-[#a6a8ad] hover:last:text-black '>
            <input
                id='search-input'
                className='flex-1 py-3 px-4 bg-search-bar text-base outline-none rounded-tl-full rounded-bl-full caret-orange-400'
                type='text'
                placeholder='Search by model name'
                onChange={handleChangeInput}
                value={enteredValue}
                onKeyDown={handleSaveValue}
            />

            <div className='h-7 w-[1px] bg-[#1618231f]'></div>
            <div
                className='flex items-center justify-center bg-search-bar py-3 px-4 rounded-tr-full rounded-br-full hover:bg-[#e4e4e6] hover:cursor-pointer'
                role='button'
            >
                <HiIcons.HiMagnifyingGlass className='text-2xl ' />
            </div>
        </div>
    )
})

export default SearchBar
