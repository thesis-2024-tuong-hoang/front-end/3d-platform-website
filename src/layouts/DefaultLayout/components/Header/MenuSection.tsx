import React, { useState, useCallback, useLayoutEffect } from 'react'
import * as AiIcons from 'react-icons/ai'
import CustomButton from '~root/components/CustomButton'
import { useLocation, useNavigate } from 'react-router-dom'
import { useAppSelector, useAppDispatch } from '~root/config/redux/reduxHooks'
import { MdKeyboardArrowDown } from 'react-icons/md'
import { AuthActions } from '~root/config/redux/slices/auth'
import { supabase } from '~root/config/supabase'
import axios from 'axios'
import { BASE_API_URL } from '~root/constants'
import { LOCAL_STORAGE_TOKEN_LOGIN, DEFAULT_USER_AVATAR } from '~root/constants'
import { Link } from '@mui/material'

export default function MenuSection() {
    const { pathname } = useLocation()
    const navigate = useNavigate()
    const [openTooltip, setOpenTooltip] = React.useState<boolean>(false)
    const dispatch = useAppDispatch()
    const authObj = useAppSelector((state) => state.auth)
    const userId = useAppSelector((state) => state.auth.currentUser)
    const [user, setUser] = useState<any>()

    const getNormalUser = useCallback(async (userId: string) => {
        const response = await axios({
            method: 'POST',
            baseURL: `${BASE_API_URL}/process-data/user`,
            data: {
                userId: userId,
            },
        })
        return response.data.user
    }, [])

    const getUserData = useCallback(async () => {
        const value = await supabase.auth.getUser()

        console.log(value.data?.user)

        if (value.data?.user) {
            const response = await axios({
                method: 'POST',
                baseURL: `${BASE_API_URL}/auth/login-with-google`,
                data: {
                    user: value.data?.user,
                },
            })

            // console.log(response)
            // console.log(value.data?.user!.id)
            const user = await getNormalUser(value.data?.user!.id)
            setUser(user)
            dispatch(
                AuthActions.login({
                    currentUser: user.id,
                    accessToken: response.data.accessToken,
                    refreshToken: response.data.refreshToken,
                    accountType: 'google',
                }),
            )
        } else {
            // const googleToken = localStorage.getItem('sb-xpvurtyfnuyuvkehgqoo-auth-token')
            const serverToken = localStorage.getItem(LOCAL_STORAGE_TOKEN_LOGIN)

            // const googleUserId = googleToken && JSON.parse(googleToken).user.id
            const serverUser = serverToken && JSON.parse(serverToken)

            // const user = await getNormalUser(googleUserId || serverUserId)
            const user = await getNormalUser(serverUser.id)

            // console.log(user)

            if (user) {
                setUser(user)
                dispatch(
                    AuthActions.login({
                        currentUser: user.id,
                        accessToken: serverUser.accessToken,
                        refreshToken: serverUser.refreshToken,
                        accountType: 'normal',
                    }),
                )
            }
        }
    }, [dispatch, getNormalUser])

    const handleToggleTooltip = () => {
        setOpenTooltip((prev) => !prev)
    }

    useLayoutEffect(() => {
        if (!user) {
            getUserData()
        }
    }, [getUserData, user])

    useLayoutEffect(() => {
        setOpenTooltip(false)
    }, [pathname])

    const navigateToLoginScreen = () => {
        navigate('/login', { replace: false })
    }

    const handleLogOut = async () => {
        dispatch(AuthActions.logout())
        navigate('/', { replace: true })
        localStorage.removeItem(LOCAL_STORAGE_TOKEN_LOGIN)
        await supabase.auth.signOut()
        await axios({
            method: 'POST',
            baseURL: `${BASE_API_URL}/auth/logout`,
            data: {
                id: userId,
            },
        })
    }

    return (
        <div className='flex items-center'>
            <CustomButton
                type='button'
                style='custom'
                className='mr-4 bg-transparent  hover:bg-[#16182308] py-3 px-3 text-black font-medium flex items-center text-lg border-[#16182333] border-solid border-[1px] hover:cursor-pointer rounded-lg transition-all'
                title='Upload'
                IconLeft={AiIcons.AiOutlinePlus}
                onClick={() => {
                    if (authObj.isAuthenticated === false) {
                        navigate('/login', { replace: false })
                    } else {
                        navigate('/upload', { replace: false })
                    }
                }}
            />

            {!authObj.isAuthenticated && (
                <CustomButton
                    onClick={navigateToLoginScreen}
                    type='button'
                    style='custom'
                    className=' bg-orange-400 hover:bg-orange-500 py-3 px-7 text-white font-medium flex items-center text-lg  hover:cursor-pointer rounded-md transition-all select-none'
                    title='Log in'
                />
            )}
            {authObj.isAuthenticated && (
                <div className='relative'>
                    <div
                        className='flex items-center cursor-pointer rounded-full p-2 border-[1px] border-solid border-black/20'
                        onClick={handleToggleTooltip}
                    >
                        <img
                            className='w-10 h-10 rounded-full select-none'
                            src={user?.avatar ? user?.avatar : DEFAULT_USER_AVATAR}
                            alt={user?.username}
                        />

                        <MdKeyboardArrowDown className='text-2xl ml-2' />
                    </div>
                    {openTooltip && (
                        <div className='w-fit   absolute top-full right-0 bg-white z-40 shadow rounded-md select-none'>
                            <p className='px-6 text-base py-3 border-b-[1px] border-solid border-black/20 '>
                                {user?.email}
                            </p>
                            <ul className='flex flex-col list-none border-b-[1px] border-solid border-black/20'>
                                <li className=' hover:bg-black/10 cursor-pointer text-black'>
                                    <Link
                                        style={{ color: 'black' }}
                                        href={`/${user?.slug}`}
                                        underline='none'
                                        className='block w-full p-3 px-6 no-underline text-black'
                                    >
                                        My profile
                                    </Link>
                                </li>
                            </ul>
                            <div
                                className='w-full p-3 px-6 hover:bg-black/10 cursor-pointer '
                                onClick={handleLogOut}
                            >
                                Sign out
                            </div>
                        </div>
                    )}
                </div>
            )}
        </div>
    )
}
