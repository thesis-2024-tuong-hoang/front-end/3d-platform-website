export default function Footer() {
    return (
        <div className='w-full h-9 border-t-[1px] max-w-7xl border-solid border-gray-400/50 py-8 flex justify-center'>
            <div className='flex items-center text-lg hover:cursor-pointer '>
                <h3 className='text-orange-400 mr-2'>3D</h3>
                <h3 className=''>Platform</h3>
            </div>
        </div>
    )
}
