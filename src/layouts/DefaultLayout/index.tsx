import React from 'react'
import Header from './components/Header'
import { Button } from '@mui/material'

export default function DefaultLayout(props: { children: React.ReactNode }) {
    return (
        <div className='w-full h-screen flex flex-col items-center'>
            <Header />
            {props.children}
        </div>
    )
}
